package football;

public class Team {

    private String sName;
    private int iTotalGames;
    private int iPoints;
    private int iScoredBalls;
    private int iMissedBalls;

    public Team(String sName, int iTotalGames, int iPoints, int iScoredBalls, int iMissedBalls) {
        this.sName = sName;
        this.iPoints = iPoints;
        this.iTotalGames = iTotalGames;
        this.iScoredBalls = iScoredBalls;
        this.iMissedBalls  = iMissedBalls;
    }

    public String getsName() {
        return sName;
    }

    public int getTotalGames() {
        return iTotalGames;
    }

    public int getPoints() {
        return iPoints;
    }

    public int getScoredBalls() {
        return iScoredBalls;
    }

    public int getMissedBalls(){
        return iMissedBalls;
    }
}