/*
Занести все команды указанного чемпионата в коллекцию
и отсортировать по количеству очков, игр, забитых и пропущенных голов. */

package football;

import java.util.*;

public class App {
    public static void main(String[] args){
        Comparator<Team> pointsComp = new PointsComparator();
        Comparator<Team> gamesComp = new TotalGamesComparator();
        Comparator<Team> scoredComp = new BallsComparator();

        Set<Team> teams = new TreeSet<>(pointsComp.thenComparing(gamesComp).thenComparing(scoredComp));

        teams.add(new Team("Гурник Забже", 22, 36, 42, 36));
        teams.add(new Team("Корона Кельце", 22, 34, 35, 28));
        teams.add(new Team("Висла Плоцк", 22, 33, 30, 29));
        teams.add(new Team("Шлёнск", 22, 27, 27, 33));
        teams.add(new Team("Легия", 22, 41, 31, 23));
        teams.add(new Team("Ягеллония", 22, 39, 31, 24));
        teams.add(new Team("Лех", 22, 37, 30, 16));
        teams.add(new Team("Заглембе", 22, 32, 33, 27));
        teams.add(new Team("Арка", 22, 32, 26, 20));
        teams.add(new Team("Висла Краков", 22, 32, 30, 29));

        teams.stream()
                .forEach(e -> System.out.println(e.getsName() + " "
                                                + e.getTotalGames() + ", "
                                                + e.getPoints() + ", "
                                                + e.getScoredBalls() + " - "
                                                + e.getMissedBalls()));
    }

}
