package football;

import java.util.Comparator;

public class BallsComparator implements Comparator<Team>{

    @Override
    public int compare(Team o1, Team o2) {

        //differense of the scored & missed goals
        int iDiff1 = o1.getScoredBalls() - o1.getMissedBalls();
        int iDiff2 = o2.getScoredBalls() - o2.getMissedBalls();

        int iResDiff = iDiff1 - iDiff2;

        //if diff equal then compare the scored goals
        return (iResDiff != 0 ? iResDiff : o1.getScoredBalls() - o2.getScoredBalls());


//        if(iDiff1 > iDiff2)
//            return 1;
//        if(iDiff1 < iDiff2)
//            return -1;
//        if(iDiff1 == iDiff2){
//            if(o1.getScoredBalls() > o2.getScoredBalls())
//                return 1;
//            if(o1.getScoredBalls() < o2.getScoredBalls())
//                return -1;
//        }
//
//        return 0;
    }

}
