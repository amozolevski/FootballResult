package football;

import java.util.Comparator;

public class TotalGamesComparator implements Comparator<Team> {
    @Override
    public int compare(Team o1, Team o2) {
        return o1.getTotalGames() - o2.getTotalGames();
    }
}
